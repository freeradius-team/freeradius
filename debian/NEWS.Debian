freeradius (3.2.7+dfsg-1) unstable; urgency=medium

  FreeRADIUS can write a log of logins to the radwtmp database meant to
  be consumed by "last". Unfortunately the format is not year-2038 safe.

  The last implementation provided by the wtmp package can only read
  the new, year-2038 safe format.

  FreeRADIUS will still write the file using the old format, but there is
  currently no implementation in Debian which can read it.

  For now we install the radlast wrapper script, but it will not work
  until the last implementation in Debian can read those old-formatted
  files. See Bug#1094356 and Bug#1095490 for details.

 -- Bernhard Schmidt <berni@debian.org>  Mon, 10 Feb 2025 23:01:18 +0100

freeradius (3.2.6+dfsg-3) unstable; urgency=medium

  The RADIUS Protocol under RFC 2865 is susceptible to forgery attacks by a
  local attacker who can modify any valid Response to any other response using
  a chosen-prefix collision attack against MD5 Response Authenticator signature.
  This attack is known as Blast-RADIUS.

  Blast-RADIUS allows a monster-in-the-middle (MitM) attacker between the
  RADIUS client and server to forge a valid protocol accept message in
  response to a failed authentication request. This forgery could give the
  attacker access to network devices and services without the attacker
  guessing or brute forcing passwords or shared secrets. The attacker does not
  learn user credentials.

  This attack is not fixed per-se, but mitigated and rendered impractical to
  carry out.

  It is worth noting that not the following use cases should be safe from a
  Blast-RADIUS attack:
  * RADIUS traffic concerns to accounting only.
  * All Access-Request packets are sent over RADIUS/TLS (RadSec).
  * RADIUS servers only doing EAP authentication.

  With this update, the default configuration is updated so that the
  Message-Authenticator attributes sent and received by the FreeRADIUS server
  are required to be in automatic mode, which will protect most RADIUS setups
  from the attack. The only RADIUS setups which are still non-protected are
  those with multiple NAS devices behind NAT. In those cases, upgrading the NAS
  devices is required.

  We encourage RADIUS administrators to read and follow the draft RFC
  draft-ietf-radext-deprecating-radius or the definitive RFC to be published
  by the IETF. Note that some equipment may not support these recommendations,
  and therefore a risk analysis should be done in all the case.

  A long term fix will need administrative actions, like the following
  non-exhaustive list (summarized from the previous draft RFC at time of
  writing):
  * Use RADIUS/TLS or RADIUS/DTLS instead of RADIUS/TCP or RADIUS/UDP. This
    should include management of cryptographic suite deprecation.
  * Administrators should use shared secrets of at least 24 octets, from a
    source of secure random numbers. Each RADIUS client should have a unique
    shared secret.
  * Proxy chains should be avoided where possible, and RFC 7585 dynamic
    discovery should be used where possible. RADIUS clients and servers
    should also be configured with static IP addresses, and static routes.
    This configuration protects them from DHCP related attacks.
  * All RADIUS traffic should use a dedicated management VLAN (isolated
    network).

 -- Bastien Roucariès <rouca@debian.org>  Fri, 09 Aug 2024 10:13:33 +0000
