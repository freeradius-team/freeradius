#!/bin/sh
# vim:ts=2:sw=2:et

set -e

case "$1" in
  configure)
    if [ -z "$2" ]; then
      # Create snakeoil certificates on initial install
      if grep -q -r 'etc/ssl/\(certs\|private\)/ssl-cert-snakeoil' /etc/freeradius; then
        if test ! -e /etc/ssl/certs/ssl-cert-snakeoil.pem || \
           test ! -e /etc/ssl/private/ssl-cert-snakeoil.key; then
          make-ssl-cert generate-default-snakeoil
        fi
        if getent group ssl-cert >/dev/null; then
          # freeradius-common dependency also provides us with adduser
          adduser --quiet freerad ssl-cert
        fi
      fi

      if grep -q -r 'dh_file = \${certdir}/dh' /etc/freeradius && \
         test ! -f /etc/freeradius/3.0/certs/dh; then
        RANDFILE=/dev/urandom openssl dhparam -out /etc/freeradius/3.0/certs/dh 1024
      fi
    fi

    # Create links for default sites, but only if this is an initial
    # install or an upgrade from before there were links; users may
    # want to remove them...
    if [ -z "$2" ]; then
      for site in default inner-tunnel; do
        if test ! -h /etc/freeradius/3.0/sites-enabled/$site && \
           test ! -e /etc/freeradius/3.0/sites-enabled/$site; then
          ln -s ../sites-available/$site /etc/freeradius/3.0/sites-enabled/$site
        fi
      done
      for module in always attr_filter chap detail detail.log \
        digest dynamic_clients eap echo exec expiration expr files \
        linelog logintime mschap ntlm_auth pap passwd preprocess \
        radutmp realm replicate soh sradutmp unix unpack utf8; do
        if test ! -h /etc/freeradius/3.0/mods-enabled/$module && \
           test ! -e /etc/freeradius/3.0/mods-enabled/$module; then
          ln -s ../mods-available/$module /etc/freeradius/3.0/mods-enabled/$module
        fi
      done
    fi
    ;;
esac

#DEBHELPER#

exit 0
